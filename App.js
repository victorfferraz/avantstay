import React from 'react';
import { Font, AppLoading } from 'expo';
import AppNavigator from './navigation/AppNavigator';
import styled from 'styled-components/native';

export const AppBlock = styled.View`
  flex: 1;
`;

class App extends React.Component {
  state = {
    isLoadingComplete: false,
  };

  loadResourcesAsync = async () => Promise.all([
    Font.loadAsync({
      'avenir-black': require('static/fonts/avenir/AvenirLTStd-Black.otf')
    }),
    Font.loadAsync({
      'avenir-medium': require('static/fonts/avenir/AvenirLTStd-Medium.otf')
    }),
    Font.loadAsync({
      'avenir-heavy': require('static/fonts/avenir/AvenirLTStd-Heavy.otf')
    }),
  ]);

  handleLoadingError = (error) => {
    console.warn(error);
  };

  handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };

  render() {
    if (!this.state.isLoadingComplete) {
      return (
        <AppLoading
          startAsync={this.loadResourcesAsync}
          onError={this.handleLoadingError}
          onFinish={this.handleFinishLoading}
        />
      );
    }
    return (
      <AppBlock
      >
        <AppNavigator />
      </AppBlock>
    );
  }

}

export default App;
