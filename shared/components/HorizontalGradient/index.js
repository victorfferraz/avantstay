import { LinearGradient } from 'expo';
import React from 'react';

export default HorizontalGradient = props => (
  <LinearGradient
    colors={props.color}
    start={{ x: 0, y: 0 }}
    end={{ x: 1, y: 0 }}
    style={props.style}
  >
    {props.children}
  </LinearGradient>
);
