import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Btn, BtnText } from './style/';

export default LargeButton = props => (
  <TouchableOpacity onPress={props.onPress}>
    <Btn>
      <BtnText>
        {props.text}
      </BtnText>
    </Btn>
  </TouchableOpacity>
);
