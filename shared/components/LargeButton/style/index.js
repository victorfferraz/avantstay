import Theme from 'shared/theme/';
import styled from 'styled-components/native';

export const Btn = styled.View`
  background-color: ${Theme.SECONDAY_COLOR}
  padding: 20px;
  width: 100%;
  border-radius: 2px;
`;

export const BtnText = styled.Text`
  color: white;
  text-align: center;
  font-size: 17px;
  line-height: 23px;
  font-family: avenir-heavy;
`;
