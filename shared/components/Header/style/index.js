import styled from 'styled-components/native';
import Theme from 'shared/theme';

export const Title = styled.Text`
  text-align: left;
  font-size: ${Theme.FONT_TITLE}px;
  color: #3B3B3C;
  width: 70%;
  font-family: avenir-heavy;
`;

export const HeaderBlock = styled.View`
  width: 100%;
  flex-direction: row;
  padding: 10px 0 24px 0;
`;
