import React from 'react';
import { Title, HeaderBlock } from './style/';
import BackButton from '../BackButton/';

export default Header = props => (
  <HeaderBlock>
    <BackButton />
    <Title>
      {props.title}
    </Title>
  </HeaderBlock>
);
