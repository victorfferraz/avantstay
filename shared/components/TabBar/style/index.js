import styled from 'styled-components/native';

export const BlockTab = styled.View`
  flex-direction: row;
  justify-content: space-around;
  background-color: #ffffff;
  shadow-color: #666;
  shadow-opacity: 0.1;
  shadow-radius: 15px;
  shadow-offset: 0px 0px;
  background-color: #fff;
  margin-top: 50px;
  padding-bottom: 20px;
`;

export const Icon = styled.View`
  padding-top: 15px;
`;

export const IconSelected = styled.View`
  margin-top: 10px;
`;
