import React from 'react';
import { TouchableOpacity, Image } from 'react-native';
import HorizontalGradient from '../HorizontalGradient/';
import { BlockTab, Icon, IconSelected } from './style/';

export default TabBar = props => (
  <BlockTab>
    <TouchableOpacity onPress={props.onPress}>
      <Icon>
        <Image
          width="23" height="18"
          source={require('static/images/nav-icon-home.png')}
        />
      </Icon>
    </TouchableOpacity>
    <TouchableOpacity onPress={props.onPress}>
      <Icon>
        <Image
          width="20" height="18"
          source={require('static/images/nav-icon-fav.png')}
        />
      </Icon>
    </TouchableOpacity>
    <TouchableOpacity onPress={props.onPress}>
      <IconSelected>
        <HorizontalGradient style={{ padding: 5, borderRadius: 20 }} color={['#357CDA', '#2886DE', '#1299E7', '#02A7EE']}>
          <Image
            width="20" height="20"
            source={require('static/images/nav-icon-main.png')}
          />
        </HorizontalGradient>
      </IconSelected>
    </TouchableOpacity>
    <TouchableOpacity onPress={props.onPress}>
      <Icon>
        <Image
          width="23" height="21"
          source={require('static/images/nav-icon-cal.png')}
        />
      </Icon>
    </TouchableOpacity>
    <TouchableOpacity onPress={props.onPress}>
      <Icon>
        <Image
          width="18" height="17.75"
          source={require('static/images/nav-icon-menu.png')}
        />
      </Icon>
    </TouchableOpacity>
  </BlockTab>
);
