import React from 'react';
import { TouchableOpacity } from 'react-native';
import { BackButtonBlock } from './style/';
import { Ionicons } from '@expo/vector-icons';
import { withNavigation } from 'react-navigation';
import Theme from 'shared/theme/';

class BackButton extends React.Component {
  render() {
    return  (
      <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}>
        <BackButtonBlock>
          <Ionicons name="ios-arrow-back" size={20} color={Theme.PRIMARY_COLOR} />
        </BackButtonBlock>
      </TouchableOpacity>);
  }
}

export default withNavigation(BackButton);
