import Theme from 'shared/theme/';
import styled from 'styled-components/native';

export const SubTitle = styled.Text`
  font-family: avenir-heavy;
  font-size: ${Theme.FONT_SUB_TITLE};
  color: ${props => props.color ? Theme.SECONDAY_COLOR : Theme.PRIMARY_COLOR};
`;

export const Container = styled.View`
  margin: 50px 16px 0 16px;
  position: relative;
`;

export const P = styled.Text`
  margin-bottom:15px;
  font-size: ${Theme.FONT_SIZE_LARGE};
  color: ${Theme.LABEL_COLOR};
  font-family: avenir-medium;
	line-height: 30px;
`;
