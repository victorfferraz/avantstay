export const houseRules = [
  {
    ruleSelected: true,
    description: 'About the Home & Neighborhood'
  },
  {
    description: 'Important Reminders'
  },
  {
    description: 'Check-in / Check-out'
  },
  {
    description: 'Parking'
  },
  {
    description: 'Wi-Fi/Password'
  },
  {
    description: 'Televisions'
  },
  {
    description: 'A/C'
  },
  {
    description: 'Laundry & Ironing'
  },
  {
    description: 'Cleaning Suppliesd'
  },
  {
    description: 'Left something behind?'
  },
];
