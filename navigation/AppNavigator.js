import { createAppContainer, createSwitchNavigator, createStackNavigator } from 'react-navigation';
import Place from '../screens/Place/';
import PlaceRules from '../screens/PlaceRules';
import PlaceRulesDetail from '../screens/PlaceRulesDetail';


const PlaceStack = createStackNavigator({
  Home: Place,
  PlaceRules,
  PlaceRulesDetail
}, {
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
});

const MainNavigator = createSwitchNavigator({
  Main: PlaceStack,
});

export default createAppContainer(MainNavigator);
