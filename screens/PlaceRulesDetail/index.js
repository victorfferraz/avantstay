import React from 'react';
import { ScrollView } from 'react-native';
import { P } from 'shared/style';
import { BlockDetail } from './style';
import { Container } from 'shared/style';
import Header from 'shared/components/Header/';


export default class PlaceRules extends React.Component {

  render() {
    return (
      <ScrollView>
        <Container>
          <BlockDetail>
            <Header navigation={this.props.navigation} title="About the Home & Neighborhood" />
            <P>
              Pack your flip-flops and swimsuit! A two-minute walk to the ocean, our home is perfect for your next beach getaway. The property includes a fully equipped kitchen, comfortable living and dining rooms, four bedrooms and two full bathrooms. Outside, you will find a great dining area with a BBQ you can use year-round.
            </P>
            <P>
              With lounge chairs looking onto the beach, a ping pong table, and all the best beach gear; you have everything you need! If you are more inclined to relax and stay indoors, enjoy the artwork on our walls which include original pieces by local artists. Contact your Guest Manager if you would like more information on the artworks.  Should you wish to explore, you are just a block from the sea; two blocks from shops and restaurants; and, a short 20 minute drive from downtown and the famous San Diego Zoo!
            </P>
          </BlockDetail>
        </Container>
      </ScrollView>
    );
  }
}
