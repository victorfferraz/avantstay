import React from 'react';
import { View, ScrollView } from 'react-native';
import HorizontalGradient from 'shared/components/HorizontalGradient/';
import LargeButton from 'shared/components/LargeButton/';
import TabBar from 'shared/components/TabBar/';
import { SubTitle, Container } from 'shared/style/';
import RoomDetailsList from './components/RoomDetailsList/';
import Tutorials from './components/Tutorials/';
import {
  Date,
  MainTitle,
  MenuContainer,
  MenuStatus,
  Name,
  Percentage,
  PlaceDetails,
  ShadowContent,
  StatusLink,
  BlockGradient,
  BlockMainImage,
  BigImage,
  BlockSubTitle,
  BlockInternetDetails,
  SmallText,
  SmallTextBlock,
  AlignBlock,
  PlaceInformation
} from './style';

const gradientStyle = { borderRadius: 5, height: 4, width: '25%' };

export default class Place extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <ScrollView bounces={false}>
        <BlockGradient>
          <HorizontalGradient style={{ height: 268 }} color={['#357CDA', '#2886DE', '#1299E7', '#02A7EE']} />
        </BlockGradient>
        <Container>
          <MenuContainer>
            <MenuStatus>
              <StatusLink status="active">
                ONGOING
              </StatusLink>
            </MenuStatus>
            <View>
              <StatusLink>
                UPCOMING
              </StatusLink>
            </View>
          </MenuContainer>
          <MainTitle>
            Your ongoing stay
          </MainTitle>
          <Date>
            Aug 26 - 31, 2018
          </Date>
          <View style={{ marginTop: 20 }}>
            <ShadowContent>
              <BlockMainImage>
                <BigImage
                  resizeMode="cover"
                  source={require('static/images/place-image.jpg')}
                />
              </BlockMainImage>
              <PlaceInformation>
                <Name>
                  Medallist
                </Name>
                <Percentage>
                  <HorizontalGradient style={gradientStyle} color={['#327EDA', '#1F8EE2', '#1199E7', '#02A7ED']} />
                </Percentage>
                <PlaceDetails>
                  5 more nights to stay
                </PlaceDetails>
                <RoomDetailsList />
                <AlignBlock>
                  <BlockSubTitle>
                    <SubTitle>
                      Door Code:
                    </SubTitle>
                    <SubTitle color="secondary" lastItem>
                      4321
                    </SubTitle>
                  </BlockSubTitle>
                </AlignBlock>
                <AlignBlock>
                  <SubTitle style={{ paddingBottom: 10 }}>
                    Wi-Fi
                  </SubTitle>
                  <BlockInternetDetails>
                    <SmallTextBlock>
                      <SmallText>
                        Network:
                      </SmallText>
                    </SmallTextBlock>
                    <SmallTextBlock lastItem>
                      <SmallText color="secondary">
                        AvantStay
                      </SmallText>
                    </SmallTextBlock>
                  </BlockInternetDetails>
                  <BlockInternetDetails>
                    <SmallTextBlock>
                      <SmallText>
                        Passowrd:
                      </SmallText>
                    </SmallTextBlock>
                    <SmallTextBlock lastItem>
                      <SmallText color="secondary">
                        GetHappy99
                      </SmallText>
                    </SmallTextBlock>
                  </BlockInternetDetails>
                </AlignBlock>
                <Tutorials />
              </PlaceInformation>
            </ShadowContent>
            <LargeButton text="Property Manual" onPress={() => this.props.navigation.navigate('PlaceRules')} />
          </View>
        </Container>
        <TabBar />
      </ScrollView>
    );
  }
}

