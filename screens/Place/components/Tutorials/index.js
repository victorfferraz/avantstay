import React from 'react';
import { SubTitle } from 'shared/style';
import { View, Text } from 'react-native';

import {
  TutorialsContainer,
  BlockTop,
  Link,
  BlockImageFeatured,
  ImageFeatured,
  Description,
  ShadowBox
} from './style';

const Tutorials = () => (
  <TutorialsContainer>
    <BlockTop>
      <View>
        <SubTitle>
          Tutorials
        </SubTitle>
      </View>
      <View>
        <Link>
          See All
        </Link>
      </View>
    </BlockTop>
    <ShadowBox>
      <BlockImageFeatured>
        <ImageFeatured
          resizeMode="contain"
          source={require('static/images/tutorial-image.jpg')}
        />
      </BlockImageFeatured>
      <Description>
        How to operate propane BBQ
      </Description>
    </ShadowBox>
  </TutorialsContainer>
);
export default Tutorials;
