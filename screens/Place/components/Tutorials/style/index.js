import Theme from 'shared/theme/';
import styled from 'styled-components/native';


export const TutorialsContainer = styled.View`
  padding-top: 40px;
`;

export const BlockTop = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-end;
`;

export const Link = styled.Text`
  font-family: avenir-heavy;
  color: ${Theme.LIGHT_TEXT_COLOR};
  font-size: ${Theme.FONT_SIZE_LARGE};
`;

export const ImageFeatured = styled.Image`
  flex: 1;
  height: undefined;
  width: undefined;
`;

export const BlockImageFeatured = styled.View`
  width: 100%;
  height: 190;
  margin-top: -10px;
`;

export const Description = styled.Text`
  color: ${Theme.LABEL_COLOR};
  font-size: ${Theme.FONT_SIZE_LARGE};
  font-family: avenir-heavy;
  text-align: center;
`;

export const ShadowBox = styled.View`
  shadow-color: #666;
  shadow-opacity: 0.1;
  shadow-radius: 15px;
  shadow-offset: 0px 10px;
  background-color: #fff;
  padding-bottom: 10px;
`;
