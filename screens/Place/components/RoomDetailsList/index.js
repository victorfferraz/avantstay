import React from 'react';
import SvgUri from 'react-native-svg-uri';
import { View } from 'react-native';
import { DetailText, NumberText, RoomDetails, RoomColumn } from './style';

const RoomDetailsList = () => (
  <View>
    <RoomDetails>
      <RoomColumn>
        <SvgUri width="24" height="24" source={require('static/images/icon-guests.svg')} />
        <NumberText>
          6
        </NumberText>
        <DetailText>
          guests
        </DetailText>
      </RoomColumn>
      <RoomColumn>
        <SvgUri width="24" height="24" source={require('static/images/icon-bathrooms.svg')} />
        <NumberText>
          3
        </NumberText>
        <DetailText>
          bathrooms
        </DetailText>
      </RoomColumn>
      <RoomColumn>
        <SvgUri width="24" height="24" source={require('static/images/icon-bedrooms.svg')} />
        <NumberText>
          5
        </NumberText>
        <DetailText>
          bedrooms
        </DetailText>
      </RoomColumn>
    </RoomDetails>
  </View>
);
export default RoomDetailsList;
