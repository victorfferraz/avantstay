import Theme from 'shared/theme/';
import styled from 'styled-components/native';

export const RoomDetails = styled.View`
  padding: 30px 30px 0px 30px;
  text-align: center;
  flex-direction: row;
  justify-content: space-between
`;

export const NumberText = styled.Text`
  font-size: ${Theme.FONT_SUB_TITLE};
  font-family: avenir-black;
  color: ${Theme.PRIMARY_COLOR};
  text-align: center;
  padding: 15px 0 3px 0;
`;

export const DetailText = styled.Text`
  font-size: ${Theme.FONT_SMALL_DETAIL};
  font-family: avenir-black;
  color: ${Theme.PRIMARY_COLOR};
  text-align: center;
`;

export const RoomColumn = styled.View`
  justify-content: center;
  align-items: center
`;

