import styled from 'styled-components/native';
import Theme from 'shared/theme/';

export const BlockGradient = styled.View`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
`;

export const Container = styled.View`
  margin: 50px 16px;
  position: relative;
`;

export const StatusLink = styled.Text`
  color: ${props => props.status ? Theme.PRIMARY_COLOR : '#ffffff'};
  opacity: ${props => props.status ? '1' : '0.75'};
  font-weight: bold;
  padding: 15px 34px 9px 34px;
  font-size: ${Theme.FONT_SIZE_SMALL};
  font-family: avenir-heavy;
`;

export const MenuStatus = styled.View`
  flex-direction: row;
  flexWrap: wrap;
  borderRadius: 22px;
  backgroundColor: #FFFFFF;
`;

export const MenuContainer = styled.View`
  flex-direction: row;
  padding:  0 22px;
`;

export const MainTitle = styled.Text`
  font-size: 26px;
  margin-top: 25px;
  font-family: avenir-black;
  color: #fff;
`;

export const Date = styled.Text`
  opacity: 0.8;
  color: #fff;
  font-family: avenir-medium;
  font-size: ${Theme.FONT_SIZE_MEDIUM};
`;

export const PlaceInformation = styled.View`
  padding: 20px 20px 40px 20px;
`;

export const BlockMainImage = styled.View`
  width: 100%;
  height: 200px;
`;

export const BigImage = styled.Image`
  flex: 1;
  width: undefined;
`;

export const Name = styled.Text`
  color: #32303D;
  font-family: avenir-heavy;
  padding-top: 10px;
  font-size: ${Theme.FONT_BIG_TITLE};
`;

export const Percentage = styled.View`
  height: 4px;
  width: 100%;
  border-radius: 5px;
  background-color: #E4E4E4;
`;

export const PlaceDetails = styled.Text`
  color: #9B9B9B;
  padding: 5px 0;
`;

export const ShadowContent = styled.View`
  shadow-color: #000;
  shadow-offset: 0px 0px;
  shadow-opacity: 0.1;
  shadow-radius: 10;
  margin-bottom: 30px;
  background: white;
  width: 100%;
  left: 0;
  top: 0;
`;

export const AlignBlock = styled.View`
  padding-top: 40px;
`;

export const BlockSubTitle = styled.View`
  flex: 1 0;
  flex-direction: row;
`;

export const SmallTextBlock = styled.View`
  padding-left: ${props => props.lastItem ? '5px' : '0px'};
`;

export const SmallText = styled.Text`
  font-family: avenir-heavy;
  font-size: ${Theme.FONT_SIZE_SUB_TEXT};
  color: ${props => props.color ? Theme.SECONDAY_COLOR : Theme.PRIMARY_COLOR};
`;

export const BlockInternetDetails = styled.View`
  flex: 1;
  flex-direction: row;
`;
