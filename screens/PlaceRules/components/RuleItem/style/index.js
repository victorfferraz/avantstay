import Theme from 'shared/theme/';
import styled from 'styled-components/native';

export const ItemDetail = styled.View`
  padding: 15px;
  flex-direction: row;
  align-items: center;
  flex-wrap: wrap
`;

export const RulesTitle = styled.Text`
  color: ${props => props.selectedItem ? 'white' : Theme.LIST_COLOR};
  font-family: avenir-heavy;
  font-size: ${Theme.FONT_SIZE_LARGE};
  padding: 0 0 0 15px;
  line-height: 22px;
  width: 70%;
`;

export const Arrow = styled.View`
  margin-left: auto;
`;
