import React from 'react';
import { ItemDetail, RulesTitle, Arrow } from './style/';
import { View, Image } from 'react-native';

export default RuleItem = props => (
  <ItemDetail>
    <View>
      <Image
        resizeMode="contain"
        source={require('static/images/icon-rules-1.png')}
      />
    </View>
    <RulesTitle selectedItem={props.selectedItem}>
      {props.rule.description}
    </RulesTitle>
    <Arrow>
      <Image
        resizeMode="contain"
        source={require('static/images/icon-arrow-gray.png')}
      />
    </Arrow>
  </ItemDetail>
);
