import styled from 'styled-components/native';

export const RuleBlock = styled.View`
  margin-bottom: 16px;
  border-radius: 8px;
  background: #fff;
  elevation: 1;
  shadow-color: #000;
  shadow-opacity: 0.1;
  shadow-radius: 15px;
  shadow-offset: 0px 10px;
`;
