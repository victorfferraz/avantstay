import React from 'react';
import { ScrollView, TouchableOpacity } from 'react-native';
import HorizontalGradient from 'shared/components/HorizontalGradient/';
import { Container } from 'shared/style/';
import { houseRules } from '../../provider/place';
import { RuleBlock } from './style/';
import RuleItem from './components/RuleItem/';
import Header from 'shared/components/Header/';

export default class PlaceRules extends React.Component {

  render() {
    return (
      <ScrollView>
        <Container>

          <Header navigation={this.props.navigation} title="House Rules" />
          {houseRules
            .map((rule, index) =>
              rule.ruleSelected ? (
                <TouchableOpacity key={index} onPress={() => this.props.navigation.navigate('PlaceRulesDetail')}>
                  <RuleBlock key={index}>
                    <HorizontalGradient style={{ borderRadius: 8 }} color={['#02A5ED', '#1695E5', '#2588DF', '#327FDB']} >
                      <RuleItem selectedItem rule={rule} />
                    </HorizontalGradient>
                  </RuleBlock>
                </TouchableOpacity>
              ) : (
                <RuleBlock key={index} >
                  <RuleItem rule={rule} />
                </RuleBlock>
              )
            )
          }
        </Container>
      </ScrollView>
    );
  }
}
