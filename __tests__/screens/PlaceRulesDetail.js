import 'react-native';
import React from 'react';
import PlaceRulesDetail from '../../screens/PlaceRulesDetail';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const tree = renderer.create(
    <PlaceRulesDetail />
    ).toJSON();
  expect(tree).toMatchSnapshot();
});
