import 'react-native';
import React from 'react';
import Place from '../../screens/Place/';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const tree = renderer.create(
    <Place />
    ).toJSON();
  expect(tree).toMatchSnapshot();
});
