import 'react-native';
import React from 'react';
import PlaceRules from '../../screens/PlaceRules';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const tree = renderer.create(
    <PlaceRules />
    ).toJSON();
  expect(tree).toMatchSnapshot();
});
