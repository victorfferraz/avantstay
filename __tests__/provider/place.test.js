
import 'react-native';

import {houseRules}  from '../../provider/place';

describe('place array', () => {
  it('should have more than one item', () => {
    expect(houseRules.length).toBeGreaterThan(0);
  });


  it('should to have 10 items', () => {
    expect(houseRules.length).toEqual(10);
  });

  it('should to have a selected item', () => {
    const selected = houseRules.filter((item) => item.ruleSelected);
    expect(selected.length).toEqual(1);
  });
});
