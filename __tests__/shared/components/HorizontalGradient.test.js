import 'react-native';
import React from 'react';
import HorizontalGradient from 'shared/components/HorizontalGradient/';
import renderer from 'react-test-renderer';

it('should reder render correctl', () => {
  const tree = renderer.create(
    <HorizontalGradient color={['#02A5ED', '#1695E5', '#2588DF', '#327FDB']} />
    ).toJSON();
  expect(tree).toMatchSnapshot();
});
