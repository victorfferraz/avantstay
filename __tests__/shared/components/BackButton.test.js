import 'react-native';
import React from 'react';
import BackButton from 'shared/components/BackButton/';

import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const tree = renderer.create(
    <BackButton />
    ).toJSON();
  expect(tree).toMatchSnapshot();
});
