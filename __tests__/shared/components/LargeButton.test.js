import 'react-native';
import React from 'react';
import LargeButton from 'shared/components/LargeButton/';

import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const tree = renderer.create(
    <LargeButton />
    ).toJSON();
  expect(tree).toMatchSnapshot();
});
