import 'react-native';
import React from 'react';
import TabBar from 'shared/components/TabBar/';

import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const tree = renderer.create(
    <TabBar />
    ).toJSON();
  expect(tree).toMatchSnapshot();
});
